
# MTMW12 assignment 3. Sammy Petch 11 October 2021
# Python code to numerically differentiate the pressure in order to calculate 
# the geostric wind relation using 2 point differencing and compare with the
# analytic solution and plot.
# Simple code without functions or dictionaries

import numpy as np
import matplotlib.pyplot as plt
from scipy import stats

# Physical parameters of the problem

pa = 1e5 # mean pressure
pb = 200 # magnitude pressure variations
f = 1e-4 # coriolis param
rho = 1  # density 
L = 2.4e6 # length scale of pressure variations k
ymin = 0 # start of y domain 
ymax = 1e6 # end of y domain

# resolution and size of the domain
N = 10 # number of intervals
dy = (ymax - ymin)/N  # length of spacing

# the spatial dimension, y:
y = np.linspace(ymin, ymax, N+1)

# arrays for the pressure, the exact geostropic wind and the wind calculated by
# numerically differenciating the pressure

p = pa + pb*np.cos(y*np.pi/L)
dp_dy = - (pb*np.pi/L)*np.sin(y*np.pi/L)
uExact =  - 1/(rho*f)*dp_dy


uNum = np.zeros_like(y)

# calculate numberical wind at end points
uNum[0] = -1/(rho*f)*(p[1] - p[0])/dy # forwards differcen
uNum[N] = -1/(rho*f)*(p[N] - p[N-1])/dy # backwards difference

# calculate numerical wind at mid point
for i in range(1,N):
    uNum[i] = -1/(rho*f)*(p[i+1] - p[i-1])/(2*dy)



# Graph to compare the numerical and analytical solutions
plt.figure(1)
plt.ylabel('u (m/s)')
plt.xlabel('y (km)')
plt.plot(y/1000, uExact, 'k-', label = 'Exact solution', lw = 1.5)
plt.plot(y/1000, uNum, '*k--', label = 'Two-point difference', ms = 10,lw= 1.5,\
         markeredgewidth = 1.5, markerfacecolor = 'none' )
plt.tight_layout()
plt.legend()
plt.show(1)

# plot the errors
plt.figure(2)
plt.plot(y/1000, uNum - uExact, '*k--',
         label ='Two-piont differences', ms=12,markeredgewidth=1.5,\
             markerfacecolor = 'none', lw= 1.5)
plt.legend()
plt.axhline(linestyle = '-', color='k', lw = 1.5)
plt.ylabel('u error (m/s)')
plt.xlabel('y (km)')
plt.show(2)

# test if the code behaves as expected 

# dy for different values of N
dy_array= np.zeros(4)
N_array = [10,20,40,80]
for i in range(1,5):
    dy_array[i-1] = (ymax - ymin)/N_array[i-1]


def error_experiment(N):
    '''
    Geostropic wind at 500km
    input: N number of intervals 
    output: error between exact solution and numerical solution
    '''
    
    dy = (ymax - ymin)/N
    y_array = np.linspace(ymin, ymax, N+1)
    
    x = int(N/2)
    y = y_array[x]
       
   # Exact at y = N/2
    
    dp_dy = - (pb*np.pi/L)*np.sin(y*np.pi/L)
    
    uExact =  - 1/(rho*f)*dp_dy    
    
    p = pa + pb*np.cos(y_array*np.pi/L)
    uNum = -1/(rho*f)*(p[x+1] - p[x-1])/(2*dy)
        
    error = uExact - uNum    
        
    return error

def error_experiment_y0(N):
    '''
    Geostropic wind at 0m
    input: N (number of intervals)
    output: error between exact solution and numerical solution
    '''
    
    dy = (ymax - ymin)/N
    y_array = np.linspace(ymin, ymax, N+1)
    
    x = 0
    y = 0
       
   # Exact at y = N/2
    
    dp_dy = - (pb*np.pi/L)*np.sin(y*np.pi/L)
    
    uExact =  - 1/(rho*f)*dp_dy    
    
    p = pa + pb*np.cos(y_array*np.pi/L)
    
    uNum = -1/(rho*f)*(p[1] - p[0])/dy
        
    error = uExact - uNum    
        
    return error

# error for dy at N/2
error = np.zeros(4)
for i in range(1,5):
    error[i-1] = error_experiment(N[i-1])
    
# error for dy at y0
error_y0 = np.zeros(4)
for i in range(1,5):
    error_y0[i-1] = error_experiment_y0(N[i-1])    



# plot log of errors
plt.figure(3)

plt.loglog(dy/1000,abs(error), 'k.-', ms = 10, label = 'at y = 0km')
plt.loglog(dy/1000,abs(error_y0), 'k*--', ms =10, label = 'at y = 500km')

plt.xlabel('dy (km)')
plt.ylabel('error (m/s)')
plt.tight_layout()
plt.xticks([10,20,40,100])
plt.title('Errors')
plt.legend()
plt.show(3)


# calculate slope of log line
m1, c1, r_value, p_value, std_err = stats.linregress(np.log(dy/1000),np.log(abs(error)))
m2, c2, r_value, p_value, std_err = stats.linregress(np.log(dy/1000),np.log(abs(error_y0)))

# plot gradients
plt.figure(4)
plt.title('gradient comparision')
plt.plot(dy/1000, (dy/1000)*m1+c1, 'k--', ms = 10, label = 'second order')
plt.plot(dy/1000,np.exp((dy/1000)*m2+c2), 'k-', ms =10, label = 'First order')
plt.xlabel('dy (km)')
plt.tight_layout()
plt.legend()
plt.show(4)


# part 2: More accurate method

uNum2 = np.zeros_like(y)

# calculate numberical wind at end points
uNum2[0] = -1/(rho*f)*(4*p[1] -p[2] - 3*p[0])/(2*dy) 
uNum2[N] = -1/(rho*f)*(p[N-2] - 4*p[N-1] + 3*p[N])/(2*dy) 

# calculate numberical wind at mid points
for i in range(1,N):
    uNum2[i] = -1/(rho*f)*(p[i+1] - p[i-1])/(2*dy)

# plot analytical solution with numerical solution
plt.figure(5)
plt.ylabel('u (m/s)')
plt.xlabel('y (km)')
plt.plot(y/1000, uExact, 'k-', label = 'Exact solution')
plt.plot(y/1000, uNum2, '*k-', label = 'second order differences', ms = 10,\
         markeredgewidth = 1.5, markerfacecolor = 'none' )
plt.tight_layout()
plt.legend()
plt.show(5)

# plot differences 
plt.figure(6)
plt.ylabel('u error (m/s)')
plt.xlabel('y (km)')
plt.axhline(linestyle = '-', color='k')
plt.plot(y/1000, uNum2 - uExact, '*k--', label = 'second order differences', ms = 10,\
         markeredgewidth = 1.5, markerfacecolor = 'none' )
plt.tight_layout()
plt.legend()
plt.show(6)


print(uNum2 - uExact)

print(uNum - uExact)